(function(angular){
    var app = angular.module('testapp', ['ngRoute']);

    app.config(function($routeProvider) {
        $routeProvider
        .when("/quote", {
          templateUrl: "templates/quote.tpl.html",
          controller: 'QuoteController'
        })
        .when("/admin", {
            templateUrl: "./templates/neweditor.tpl.html",
            controller: 'NewsController'
          })
        .when("/home", {
          templateUrl: "templates/home.tpl.html"
        })
        .otherwise({
            redirectTo: '/home'
        });
      });


   app.controller('QuoteController', ['$http', '$scope', function($http, $scope){

      var quoteApiUrl = "https://j950rrlta9.execute-api.us-east-2.amazonaws.com/v1/ArgoChallenge";
      $scope.quoteData = {};
      $scope.submitQuote = submitQuote;

      function submitQuote(){
          $http.post(quoteApiUrl, {
            owner_name: $scope.quoteData.owner_name,
            model: $scope.quoteData.model,
            seat_capacity: $scope.quoteData.seat_capacity,
            manufactured_date: $scope.quoteData.manufactured_date,
            purchase_price: $scope.quoteData.purchase_price,
            broker_email: $scope.quoteData.broker_email
          }, 
          {
            headers: {
              "x-api-key": "L0Q3GvXCwB9jVSmvaJbw5augw4xHCvMy4Egqim2p",
              "content-type": "application/json"
            }
          })
          .then(function(res){
              console.log(res);
          },
          function(err){

          })
      }

   }]);

   app.controller('NewsController', ['$http', '$scope', function($http, $scope){

      $scope.title = "News Admin Panel"
      var baseUrl = "http://localhost:4001/api/newsPost";
      $scope.edit = false;
      $scope.newslist = [];
      $scope.news = {};

      $scope.createNewsPost = createNewsPost;

      function getNewsPost(){
        $http.get(baseUrl).then(function(res){
          console.log(res);
          $scope.newslist = res.data.data.docs;
        }, function(err){

        })
      }

      function createNewsPost(){
        $http.post(baseUrl, {title: $scope.news.title, description: $scope.news.description}).then(function(res){
          console.log(res);
          getNewsPost();
        }, function(err){

        })
      }

      $scope.updateNewsPost = function(){

        $http.put(baseUrl, {_id: $scope.news.id, title:$scope.news.title, description:$scope.news.description}).then(function(res){
          console.log(res);
          getNewsPost();
          $scope.news = {};
          $scope.edit = false;
        }, function(err){

        })
      }
    

    $scope.makeEditable = function(newsObj){

       $scope.edit = true;
       $scope.news = {};
       $scope.news.id = newsObj._id;
       $scope.news.title = newsObj.title;
       $scope.news.description = newsObj.description;
    }

     


    $scope.delete = function (id) {

        $http.put(baseUrl+'/delete', {id:id}).then(function(res) {
          console.log(res);
          getNewsPost();
        }, function(err){

        });
    }

      getNewsPost();

   }]);
})(angular);