'use strict';
var errorMessages = require('./errorMessages.json');

var response = function (res, statusCode, data, message) {

    if(errorMessages.errorCodes[statusCode]) {
        var message = message || errorMessages.errorCodes[statusCode].message;

        if(!!data){
            res.json({"status": statusCode, "message":message, "data":data});
        }
        else {
            res.json({"status": statusCode, "message": message});
        }
        
    }
    else {
        res.json({"status": 500, "message": (message || "Internal Server Error.")});
    }
};

module.exports = {
        response: response
};