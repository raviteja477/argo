var mongoose = require('mongoose');

var initDBCon = function(config){
	var connectDB = function(cb) {
		
		mongoose.connect(config.dbConnectionStr, function (err, res) {
			
			if(err){
				console.log("failed to connect with database");
				return false;
			}

	      	cb(err, res);
	     }); 
	};

	return {
		connectDB:connectDB
	}
};



module.exports = exports = {
	initDBCon: initDBCon
};