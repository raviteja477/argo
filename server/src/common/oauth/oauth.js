var jwt = require('jsonwebtoken');

var responseObject = {
  status: 'Access Denied',
  message: 'You are not authorized to access this resource',
  status: 403
};

var initOAuth = function(config) {

    var authenticate = function(req, res, next) {
        var token = req.body.token || req.query.token || req.headers['x-access-token'];
       
        if (token) {
          jwt.verify(token, config.secret, function(err, decoded) {      
            if (err) {
              return res.status(403).send({ 
                    message: "unauthorised", 
                    errorCode: 403 
                });    
            } else {
                req.decoded = decoded;    
                next();
            }
          });

        } else {

          return res.status(403).send({ 
              message: "unauthorised", 
              errorCode: 403 
          });
        }
      };

    return {authenticate: authenticate};
}

module.exports = {
    initOAuth: initOAuth
}