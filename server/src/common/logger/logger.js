var winston = require("winston"); 
var moment = require("moment");
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
var fs = require('fs');
var path = "logs";

if (!fs.existsSync(path)){
    fs.mkdirSync(path);
}

var auditSchema = new Schema({
	type: String,
	api: String,
	request: Object,
	response: Object,
	createdOn: String
});

var auditModel = mongoose.model('Audit', auditSchema);

var updateLogInDB = function(auditModel, auditObj) {
	var auditDoc = new auditModel(auditObj);
	auditDoc.save();
};

var logger = new winston.Logger({
 level: 'info',
 transports: [
		 new (winston.transports.Console)({
			 	level: 'debug',
	            handleExceptions: true,
	            json: false,
	            colorize: true,
	            timestamp: true,
	            exitOnError: false,
	            timestamp: function() {
			        return moment().format('YYYY-MM-DD HH:mm:ss.SSSS');
			    },
			    formatter: function(options) {
			        // Return string will be passed to logger.
				var auditDocObj = JSON.parse(options.message);

			        if(!!auditDocObj && typeof auditDocObj === "object") {

				        auditDocObj.createdOn = options.timestamp();
				        	
				        updateLogInDB(auditModel, auditDocObj);
			        }

			        return options.timestamp() +' '+ options.level.toUpperCase() +' '+ (options.message ? options.message : '') +
			          (options.meta && Object.keys(options.meta).length ? '\n\t'+ JSON.stringify(options.meta) : '' );
			    }
		 }),
		 new (winston.transports.File)({
			      name: 'audit-file',
			      filename: path + '/audit.log',
			      handleExceptions: true,
			      level: 'info',
			      exitOnError: false,
			      timestamp: function() {
			        return moment().format('YYYY-MM-DD HH:mm:ss.SSSS');
			      },
			      formatter: function(options) {
			        // Return string will be passed to logger.
			        //updateLogInDB(auditModel, {createdOn:options.timestamp(), });

			        return options.timestamp() +' '+ options.level.toUpperCase() +' '+ (options.message ? options.message : '') +
			          (options.meta && Object.keys(options.meta).length ? '\n\t'+ JSON.stringify(options.meta) : '' );
			      }
		}),
		new (winston.transports.File)({
			      name: 'error-file',
			      filename: path +'/error.log',
			      level: 'error',
			      handleExceptions: true,
			      level: 'info',
			      exitOnError: false,
			      timestamp: function() {
			        return moment().format('YYYY-MM-DD HH:mm:ss.SSSS');
			      },
			      formatter: function(options) {
			        // Return string will be passed to logger.
			        return options.timestamp() +' '+ options.level.toUpperCase() +' '+ (options.message ? options.message : '') +
			          (options.meta && Object.keys(options.meta).length ? '\n\t'+ JSON.stringify(options.meta) : '' );
			      }
		})
	 ]	
 });

module.exports = logger;
