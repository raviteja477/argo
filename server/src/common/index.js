var logger = require('./logger/logger');
var errorHandler = require('./errorHandler/errorHandler');
var oauth = require('./oauth/oauth');
var dbCon = require('./dbcon/dbCon');

var common = function(config){
	var commonModule = {
		logger:logger,
		errorHandler: errorHandler
	}
	
	if(!!config && !!config.authInfo){
		commonModule.oauth = oauth.initOAuth(config.authInfo);
	}
	if(!!config && !!config.dbInfo){
		commonModule.dbCon= dbCon.initDBCon(config.dbInfo)
	}
	return commonModule;
}

module.exports = common;