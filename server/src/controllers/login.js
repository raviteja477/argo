var auth = require('../models/auth').authModel;
var authConfig = require('../server/config/config');
var jwt    = require('jsonwebtoken');
var login = function(req, res) {

		if (!!req.body && req.body.username && req.body.password && req.body.password === 'testpass') {
			
			var user = req.body;
			var token = jwt.sign(user, authConfig.secret, {});
			
			res.json({"status":200, "message":"Success", "token":token});
		} 
		else if(!!req.body && req.body.username && req.body.password){
			res.json({"status":404, "message":"No record found"});
		}
		else {
			res.json({"status":101, "message":"Error on Missing Input Parameter in Request Data Json"});
		}
};

var verifyLogin = function(req, res) {
	res.json({'message':'User Is Logged In', 'errorCode':200});			
};

module.exports = exports = {
	login: login,
	verifyLogin: verifyLogin
};