var express = require('express');
var mainRouter = express.Router();
var newsPost = require('./newsPostController');

var router = function() {

 	//User Group middleware
 	 mainRouter.post('/newsPost', newsPost.create);
     mainRouter.get('/newsPost', newsPost.list);
     mainRouter.get('/newsPost/:newsPostId', newsPost.read);
     mainRouter.put('/newsPost', newsPost.update);
     mainRouter.put('/newsPost/delete', newsPost.remove);
     mainRouter.param('newsPost', newsPost.newsPostByID);

    return mainRouter;
};

module.exports = router;

