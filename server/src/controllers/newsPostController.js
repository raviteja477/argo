'use strict';

var mongoose = require('mongoose'),
    NewsPost = require('../models/newsPostModel').newsPostModel,
    response = require('../common')().errorHandler.response;

//Create new user group
exports.create = function(req, res) {
    console.log(req.body);
    var newsPost = new NewsPost(req.body);
    var error = newsPost.validateSync(); 

    // Register timestamp
    newsPost.createdDate = Date.now();
    newsPost.lastUpdatedDate = Date.now();

    //Check mandatory fields 
    if (!error) {
        newsPost.save(function(err) {
            if (err) {
                if (err.code !== 11000) {
                    return response(res, 500);
                } else
                    return response(res, 104);
            } else
                response(res, 200);
        });
    } else
        return response(res, 101);
};

//Returns list of news
exports.list = function(req, res, next) {
    var queryParams, page, limit, query = {};

    if (!req.query.page && !req.query.limit) {
        queryParams = { page: 1, limit: 10 };
    } else if (req.query.page && req.query.limit) {
        queryParams = { page: parseInt(req.query.page), limit: parseInt(req.query.limit) };
    } else {
        queryParams = {};
    }
    
    if (!!req.query.active) {
        query = { active: req.query.active };
        NewsPost.find(query, function(err, result) {
            if (err) {
                return response(res, 500);
            } else {
                response(res, 200, result);
            }
        });
    } else {
        NewsPost.paginate(query, queryParams, function(err, result) {
            if (err)
                return response(res, 500);
            else
                response(res, 200, result);
        });
    }
};


exports.read = function(req, res, next) {
    // convert mongoose document to JSON
    var group = req.group ? req.group.toJSON() : {};

    response(res, 200, group);
};


exports.update = function(req, res) {
    var newsPost = new NewsPost(req.body);
    var error = newsPost.validateSync();
    var id = req.body._id;
    if (!mongoose.Types.ObjectId.isValid(id)) {
        return response(res, 400);
    }

    //update timestamp
    newsPost.lastUpdatedDate = Date.now();

    if (!error) {
        NewsPost.findByIdAndUpdate({
            _id: id
        }, newsPost , function(err, news) {
            if (err) {
                if (err.code !== 11000) {
                    return response(res, 500);
                } else
                    return response(res, 104);
            }
            else if (!news) {
                return response(res, 404);
            }
                response(res, 200);
        });
    } else
        return response(res, 101);
};


exports.remove = function(req, res) {

    NewsPost.remove({ _id: req.body.id }, function(err) {
        if (err) {
                if (err.code !== 11000) {
                    return response(res, 500);
                } else
                    return response(res, 104);
            }
            
                response(res, 200);
    });

    // var newsPost = new NewsPost(req.body);
    // var error = newsPost.validateSync();
    // var id = req.body._id;
    // if (!mongoose.Types.ObjectId.isValid(id)) {
    //     return response(res, 400);
    // }

    // //update timestamp
    // newsPost.lastUpdatedDate = Date.now();

};

/*news middleware
 */
exports.newsPostByID = function(req, res, next, id) {
    if (!mongoose.Types.ObjectId.isValid(id)) {
        return response(res, 400);
    }

    NewsPost.findById(id, function(err, news) {
        if (err)
            return response(res, 500);
        else if (!news) {
            return response(res, 404);
        }
        req.news = news;
        next();
    });
};
