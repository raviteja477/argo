  'use strict';
//import nodejs modules
var express = require('express');
var router = express.Router();
var cors = require('cors');
var app = express();
var bodyParser = require('body-parser');
var jwt    = require('jsonwebtoken');
var acl =  require('express-acl');

//import custom modules
var config = require('./config/config.json');
var common = require('../common')(config);
var logger = common.logger;

//middlewares
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
app.use(cors());

app.use(function(req,res,next){
    var auditLogObj = {
                type: req.method,
                api: req.url,
                request: req.body,
              };
    logger.info(JSON.stringify(auditLogObj));
    next();
 }); 

var route = require('../controllers');
router.use(route());
app.use('/api', router);


var initServer = function() {
	var port = process.env.PORT || 4001;
	app.listen(port);
	console.log('server started on port ' + port);
};

common.dbCon.connectDB(initServer);

module.exports = app;
