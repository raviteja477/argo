'use strict';

var mongoose = require('mongoose'),
	Schema = mongoose.Schema,
	mongoosePaginate = require('mongoose-paginate');

//User Group Schema
var newsPostSchema = new Schema({
    title: { type: String, required: true},
    description: String,
    createdDate: { type: Date},
    lastUpdatedDate: { type: Date}
});

newsPostSchema.plugin(mongoosePaginate);

var newsPostModel = mongoose.model('NewsPost', newsPostSchema);

module.exports = {
    newsPostModel: newsPostModel
};