var mongoose = require('mongoose');
var authSchema = mongoose.Schema;

var authSchema = new authSchema({
	emailAddress : { type: String, required: true, unique: true},
  password: {type: String, required:true}
});

var authModel = mongoose.model('auth', authSchema);

module.exports = exports = {
	authModel: authModel
}
