// Include gulp
var gulp = require('gulp');

// Include Our Plugins
var jshint = require('gulp-jshint');

// Lint Task
gulp.task('lint', function() {
    return gulp.src('src/**/**/*.js')
        .pipe(jshint({
        		node : true
        	}))
        .pipe(jshint.reporter('default'));
});
// Default Task
gulp.task('default', ['lint']);